import angular from 'angular';

// for loading styles we need to load main css file
import './styles/styles.css';

// loading shared module


// loading all module components
import './app.components';

const appModule = angular
	.module('app', [
		// shared module

		// 3rd party modules

		// application specific modules
		'app.counter'
	]);

export default appModule;
