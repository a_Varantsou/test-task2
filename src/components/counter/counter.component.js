import './counter.css';

import template from './counter.html';
import controller from './counter.controller';

export default {
	template,
	controller
};
