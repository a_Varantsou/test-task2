export default class CounterController {
	constructor($log) {
		'ngInject';
		this.$log = $log;

		this.operationTypes = ['+', '-'];
	}

	$onInit = () => {
		this.result = 0;

		this.rows = [];
		this.addRow();
		this.$log.info('Activated Counter View.');
	};

	addRow() {
		this.rows.push({ value: 0, disabled: false, operationType: '+' });
	};

	removeRow(index) {
		this.rows.splice(index, 1);
		this.reflowCounter();
	}

	disableRow(index) {
		this.rows[index].disabled = !this.rows[index].disabled;
		this.reflowCounter();
	}

	reflowCounter() {
		const operationsSwitch = (base, value, operation) => {
			switch (operation) {
				case '+':
					return base + value;
				case '-':
					return base - value;
			}
		};
		this.result =
			this.rows.reduce((acc, item) =>
				(!item.disabled && item.value ? operationsSwitch(acc, item.value, item.operationType) : acc), 0);
	}
}
