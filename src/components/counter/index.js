import counterComponent from './counter.component';

const counterModule = angular.module('app.counter', []);

// loading components, services, directives, specific to this module.
counterModule.component('appCounter', counterComponent);

// export this module
export default counterModule;
